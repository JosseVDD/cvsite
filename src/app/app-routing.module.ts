import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from './home/home.component';
import { ContactComponent} from './contact/contact.component';
import { SkillsComponent} from './skills/skills.component';
import {AboutMeComponent} from './about-me/about-me.component';
import { ErrorComponent } from './error/error.component';

const routes: Routes = [
  {path : '', component: HomeComponent},
  {path : 'about-me', component: AboutMeComponent},
  {path : 'skills', component: SkillsComponent},
  {path : 'contact', component: ContactComponent}, 
  {path : '**' , component: ErrorComponent}
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents =[HomeComponent, AboutMeComponent, SkillsComponent, ContactComponent]
